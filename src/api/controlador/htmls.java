package api.controlador;

public class htmls {
	
	//servlet direcciones
	public static final String HOME = "/index.jsp";
	public static final String INSTITUCION = "/institucion";
	public static final String GALERIA = "/galeria";
	public static final String CONTACTENOS = "/contactenos";
	public static final String COMUNICADOS = "/comunicados";
	public static final String LOGIN = "/login";
	public static final String UNLOGIN = "/login/out";
	public static final String REGISTRER = "/login/registrar";
	public static final String CALENDARIO = "/calendario";
	public static final String ADMIN_HOME_PAGE = "/admin/adminHomePage";
	public static final String ADMIN_ADMI_CURSO = "/admin/Cursos";
	public static final String ADMIN_ADMI_USUARIOS = "/admin/Usuarios";
	public static final String ADMIN_ADMI_MATRICULAR = "/admin/Matriculas";
	public static final String ADMIN_ADMI_ASIGNAR = "/admin/asignar";
	public static final String ADMIN_ADMI_CALENDARIO = "/admin/calendario";
	public static final String ADMIN_ADMI_CONTACTENOS = "/admin/contactenos";
	
	//imagenes direcciones
	public static final String LOGO_IMG = "/images/logo.jpg";
	public static final String IMG_1 = "/images/imgc1.jpg";
	public static final String IMG_2 = "/images/imgc2.jpg";
	public static final String IMG_3 = "/images/imgc3.jpg";
	
	//jsp 
	public static final String REGISTRAR_JSP = "/registrar.jsp";
	public static final String PAGE_DENNY_JSP = "/paginaNoAutorisada.jsp";
	public static final String PAGE_NO_FOUND_JSP = "/paginaNoEncontrada.jsp";
	public static final String LOGIN_JSP = "/login.jsp";
	public static final String ERROR_JSP = "/error.jsp";
	public static final String ADMIN_HOME_JSP = "/WEB-INF/admin/home.jsp";
	public static final String ADMIN_USUARIOS_JSP = "/WEB-INF/admin/usuario.jsp";
	public static final String ADMIN_CURSOS_JSP = "/WEB-INF/admin/cursos.jsp";
	public static final String ADMIN_ASIGNAR_JSP = "/WEB-INF/admin/asignar.jsp";
	public static final String ADMIN_MATRICULAR_JSP = "/WEB-INF/admin/matricular.jsp";
	//js direcciones
	public static final String JQUERY_JS = "/js/jquery.js";
	public static final String CYCLE_JS = "/js/cycle.js";
	public static final String JQUERYFUNCTIONS_JS = "/js/jqueryFunctions.js";
	public static final String JQUERY_LATEST_JS = "/js/jquery-latest.js";
	public static final String RESPONSIVES_LIDES_JS = "js/responsiveslides.min.js";
	public static final String MAIN_JS = "js/main.js";
	
	//hoja de estilos
	public static final String ESTILOS_CSS = "/estilos/estilos.css";
	public static final String FLEXBOXGRID_CSS = "/estilos/flexboxgrid.css";
	public static final String ICONOS_CSS = "/estilos/iconos.css";
	
	//html
	public static final String HEAD = "<head>\n";
	public static final String LHEAD = "</head>\n";
	public static final String BODY = "<body>\n";
	public static final String LBODY = "</body>\n";
	public static final String HTML = "<html>\n";
	public static final String LHTML = "</html>\n";
	public static final String TABLE = "<table>\n";
	public static final String LTABLE = "</table>\n";
	public static final String TR = "<tr>";
	public static final String LTR = "</tr>";
	public static final String TD = "<td>";
	public static final String LTD = "</td>";
	public static final String BR = "<br>";
	public static final String A = "<a>";
	public static final String LA = "</a>";
	
	public static final String PORTADA = ""
			+"<header>\n"
			+"	<div class=\"container\">\n"
			+"		<div class=\"row\">\n"
			+"			<div class=\"col-xs-12 tit-pagina\">\n"
			+"				<h1>Juan Jacobo <span>Rousseau</span></h1>\n"
			+"			</div>\n"
			+"		</div>\n"
			+"	</div>\n"
			+"	<div class=\"logoc\"></div>\n"
			+"	<div class=\"logo\"></div>\n"
			+"</header>\n";
	public static String li_login_dinamico = "  	      <li><a href=\""+LOGIN+"\"><span class=\"icon-user\"></span>Login</a></li>\n";
	public static String getMenu(){
			String MENU = ""
			+"<div class=\"container\">\n"
			+"	<div class=\"row\">\n"
			+"  	<div class=\"col-xs\">\n"
			+"  	  <div class=\"menu-bar\">\n"
			+"  	    <a href='' class=\"bt-menu\"><span class=\"icon-align-justify\"></span>Menu</a>\n"
			+"  	  </div>\n"
		    +"  	  <div class=\"menu-p\">\n"
		    +"  	    <ul>\n"
		    +"  	      <li><a href=\""+HOME+"\"><span class=\"icon-home\" ></span>Inicio</a></li>\n"
		    +"  	      <li><a href=\""+INSTITUCION+"\"><span class=\"icon-flag\"></span>Nuestra Institución</a></li>\n"
		    +"  	      <li class=\"submenu\">\n"
		    +"  	        <a href=\"\"><span class=\"icon-open-book\"></span>Areas<span class=\"caret icon-chevron-down\"></span></a>\n"
		    +"  	        <ul class=\"children\">\n"
		    +"  	          <li><a href=\"\">Inicial</a></li>\n"
		    +"  	          <li><a href=\"\">Primaria</a></li>\n"
		    +"  	          <li><a href=\"\">Secundaria</a></li>\n"
		    +"  	        </ul>\n"
		    +"  	      </li>\n"
		    +"  	      <li><a href=\"\"><span class=\"icon-camera\"></span>Galería</a></li>\n"
		    +"  	      <li><a href=\""+CONTACTENOS+"\"><span class=\"icon-phone\"></span>Contacto</a></li>\n"
		    +"  	      <li><a href=\""+COMUNICADOS+"\"><span class=\"icon-speech-bubble\"></span>Comunicados</a></li>\n"
		    +"  	      <li><a href=\""+CALENDARIO+"\"><span class=\"icon-news\"></span>Calendario Escolar</a></li>\n"
		    +li_login_dinamico
		    +"  	    </ul>\n"
		    +"  	  </div>\n"
		    +"  	</div>\n"
		    +"	</div>\n"
		    +"</div>\n";
			return MENU;
	}
	
	public static final String SLIDESHOW = ""
			+"<div class=\"container\">\n"
			+"<div class=\"row\">\n"
			+"  <div class=\"col-xs-12 callbacks_container\">\n"
			+"    <ul class=\"rslides\" id=\"slider\">\n"
			+"      <li>\n"
		    +"        <img src=\"images/imgc1.jpg\" alt=\"\">\n"
		    +"        <p class=\"caption\">TRABAJANDO UNIDOS POR UNA EDUCACIÓN CONSCIENTE</p>\n"
		    +"      </li>\n"
		    +"      <li>\n"
		    +"        <img src=\"images/imgc2.jpg\" alt=\"\">\n"
		    +"        <p class=\"caption\">TRABAJANDO UNIDOS POR UNA EDUCACIÓN CONSCIENTE</p>\n"
		    +"      </li>\n"
		    +"      <li>\n"
		    +"        <img src=\"images/imgc3.jpg\" alt=\"\">\n"
		    +"        <p class=\"caption\">TRABAJANDO UNIDOS POR UNA EDUCACIÓN CONSCIENTE</p>\n"
		    +"      </li>\n"
		    +"    </ul>\n"
		    +"  </div>\n"
		    +"  <div class=\"col-xs-12 \"></div>\n"
		    +"</div>\n"
		    +"</div>\n";
	public static final String SPACE = "<div class=\"space\"></div>";
	public static final String FOOTER = ""
			+"<footer class=\"footer\">\n"
			+"	<div class=\"fleft\"><p>Copyright statement.</p></div>\n"
			+"	<div class=\"fright\"><p>Diseñado por: Dewitt Chavez, Henry Talavera, Milagros Mayta, Eduardo Sutta</p></div>\n"
			+"	<div class=\"fclear\"></div>\n"
			+"</footer>\n";
	public static String getMenu_Admin(){
			String MENU_ADMIN = ""
			+"<div id=\"menu_admin\">\n"
			+"	<ul>\n"
			+"		<li><a href="+ADMIN_HOME_PAGE+">Home admin</a></li>\n"
			+"		<li><a href="+ADMIN_ADMI_CURSO+">CRUD cursos</a></li>\n"
			+"		<li><a href="+ADMIN_ADMI_USUARIOS+">CRUD usuarios</a></li>\n"
			+"		<li><a href="+ADMIN_ADMI_MATRICULAR+">Matricular alumnos</a></li>\n"
			+"		<li><a href="+ADMIN_ADMI_ASIGNAR+">Asignar curcos profesor</a></li>\n"
			+"		<li><a href="+ADMIN_ADMI_CALENDARIO+">CRUD eventos calendario</a></li>\n"
			+"		<li><a href="+ADMIN_ADMI_CONTACTENOS+">CRUD contactenos</a></li>\n"
			+li_login_dinamico
			+"	</ul>\n"
			+"</div>\n";
			return MENU_ADMIN;
	}
	
	public static String HEAD_ADMIN = ""
			+HEAD
			+"<title>Administrador</title>\n"
			+"<script src="+JQUERY_LATEST_JS+"></script>\n"
			+"<script type=\"text/javascript\" src="+JQUERYFUNCTIONS_JS+"></script>\n"
			+LHEAD;
	
	public static void session_active(){
		li_login_dinamico = "		  <li><a href=\""+UNLOGIN+"\"><span class=\"icon-user\"></span>unlogin</a></li>\n";
	}
	public static void session_disable(){
		li_login_dinamico = "  	      <li><a href=\""+LOGIN+"\"><span class=\"icon-user\"></span>Login</a></li>\n";
	}
	public static void session_active(String user){
		li_login_dinamico = "		  <li><a href=\""+UNLOGIN+"\"><span class=\"icon-user\"></span>usu("+user+")</a></li>\n";
	}
	public static String msj_alerta(String msj){
		return "<script>console.log(alert(\""+msj+"\"));</script>";
	}

}
