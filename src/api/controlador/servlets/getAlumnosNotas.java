package api.controlador.servlets;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import javax.jdo.Query;

import api.modelo.PMF;
import api.modelo.tablas.*;

@SuppressWarnings("serial")
public class getAlumnosNotas extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		String codCurso = req.getParameter("curso");
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Key key = KeyFactory.stringToKey(codCurso);
		Curso cur = pm.getObjectById(Curso.class, key);
		Query q = pm.newQuery(NotasCursos.class);
		try{
			q.setFilter("CURSO == cursoParam");
			q.setOrdering("ALUMNO descending");
			q.declareParameters("Curso cursoParam");
			List<NotasCursos> ln = (List<NotasCursos>) q.execute(cur); 
			req.setAttribute("lista_notas", ln);
			req.setAttribute("curso", cur);
			RequestDispatcher rd = req.getRequestDispatcher("listaNotas.jsp");
			rd.forward(req, resp);
		}
		catch(Exception e){
			
		}
		finally{
			q.closeAll();
		}
		
		
	}
}
