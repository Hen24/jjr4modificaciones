package api.controlador.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import api.controlador.htmls;
import api.modelo.PMF;
import api.modelo.tablas.Cuenta;
import api.modelo.tablas.Curso;
import api.modelo.tablas.NotasCursos;
import api.modelo.tablas.Persona;

@SuppressWarnings("serial")
public class AsignarCursoServlet extends HttpServlet{
	
	@SuppressWarnings("unchecked")
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException{
		String prof = req.getParameter("prof");
		String[] curs = req.getParameterValues("elegidos");
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try{
			Key key = KeyFactory.stringToKey(prof); 
			Persona profe = pm.getObjectById(Persona.class, key);
			
			for(int i=0; i<curs.length;i++){
				Key kc = KeyFactory.stringToKey(curs[i]);
				Curso cactual = pm.getObjectById(Curso.class, kc);
				cactual.setProfesor(profe);
				
				/*
				Query q = pm.newQuery(Curso.class);
				q.setFilter("ASIGNADO == false");
				q.setOrdering("KEY descending");
				
				Query q1 = pm.newQuery(Curso.class);
				q1.setFilter("ASIGNADO == true");
				q1.setOrdering("KEY descending");
				
				Query q2 = pm.newQuery(Persona.class);
				q2.setOrdering("KEY descending");
				q2.setFilter("TIPO == 3");
				
					@SuppressWarnings("unchecked")
					Collection cursosnoasig = (Collection) q.execute();
					req.setAttribute("cursosnoasig", cursosnoasig);
					Collection cursosasig = (Collection) q1.execute();
					req.setAttribute("cursosasig", cursosasig);
					
					Collection profesores = (Collection) q2.execute();
					req.setAttribute("profesores", profesores);
					req.getRequestDispatcher(htmls.ADMIN_ASIGNAR_JSP).forward(req, resp);
				*/
			}
		}catch(Exception e){
			System.out.println(e);
		}finally{
			Query q = pm.newQuery(Curso.class);
			q.setFilter("ASIGNADO == false");
			q.setOrdering("KEY descending");
			
			Query q1 = pm.newQuery(Curso.class);
			q1.setFilter("ASIGNADO == true");
			q1.setOrdering("KEY descending");
			
			Query q2 = pm.newQuery(Persona.class);
			q2.setOrdering("KEY descending");
			q2.setFilter("TIPO == 3");
			
				@SuppressWarnings("unchecked")
				Collection cursosnoasig = (Collection) q.execute();
				req.setAttribute("cursosnoasig", cursosnoasig);
				Collection cursosasig = (Collection) q1.execute();
				req.setAttribute("cursosasig", cursosasig);
				
				Collection profesores = (Collection) q2.execute();
				req.setAttribute("profesores", profesores);
				req.getRequestDispatcher(htmls.ADMIN_ASIGNAR_JSP).forward(req, resp);
			
		}
	}
}