package api.controlador.servlets;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import api.controlador.htmls;
import api.modelo.PMF;
import api.modelo.tablas.Cuenta;
import api.modelo.tablas.Curso;
import api.modelo.tablas.NotasCursos;
import api.modelo.tablas.Persona;

@SuppressWarnings("serial")
public class crearUsuarioServlets extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		String name = req.getParameter("nombre");
		String lastname = req.getParameter("apellidos");
		String dni = req.getParameter("dni");
		String user = name.substring(0, 1) + lastname.substring(0, 1) + dni;
		String pass = dni;
		short tipo = (short) Integer.parseInt(req.getParameter("tipo"));
		String grado = req.getParameter("grado");
		Cuenta newCuenta = new Cuenta(user,pass);
		Persona newPerson = new Persona(name,lastname,dni);
		newCuenta.setPersona(newPerson);
		newPerson.setTIPO(tipo);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		//pm.makePersistent(newPerson);
		pm.makePersistent(newCuenta);
		
		if(tipo == 2){
			Query qe =pm.newQuery(Curso.class);
			qe.setFilter("GRADO == grade && NIVEL == niv");
			qe.declareParameters("String grade, String niv");
			List<Curso> c = (List<Curso>) qe.execute(grado.substring(0, 1), grado.substring(1, 2));
			System.out.println(grado.substring(0, 1) + grado.substring(1, 2));
			for(Curso cs : c){
				System.out.println(cs.getNombres());
				NotasCursos nuevo = new NotasCursos();
				pm.makePersistent(nuevo);
				nuevo.setAlumno(newPerson);
				nuevo.setCurso(cs);
			}
		}

		
		Query q = pm.newQuery(Cuenta.class);
		q.setOrdering("KEY descending");
		try{
			@SuppressWarnings("unchecked")
			Collection cuentas = (Collection) q.execute();
			req.setAttribute("cuentas", cuentas);
			req.getRequestDispatcher(htmls.ADMIN_USUARIOS_JSP).forward(req, resp);
			
			
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			q.closeAll();
		}
	}
}
