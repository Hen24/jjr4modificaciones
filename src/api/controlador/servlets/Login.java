package api.controlador.servlets;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import api.controlador.htmls;
import api.modelo.PMF;
import api.modelo.tablas.*;

@SuppressWarnings("serial")
public class Login extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession sesion = request.getSession();
        String usu, pass;
        usu = request.getParameter("user");
        pass = request.getParameter("password");
        PersistenceManager pm = PMF.get().getPersistenceManager();
       // request.getSession().setAttribute("servletMsg", "Retroalimentaci�n"); 
       //response.sendRedirect("/ejemplo.jsp"); 
        //<% 
        //String msg = (String)session.getAttribute("servletMsg"); 
        //%> 
        try{
        	Admin admin = Admin.getAdmin();
        	if(admin.isCuenta(usu, pass)){
        		sesion.setAttribute("admin", admin);
        		htmls.session_active(admin.getCuenta());
        		response.sendRedirect(htmls.ADMIN_HOME_PAGE);
        	}
        	else{
        		
        		/*
        		 * FALTA IMPoLEMENTAR ESTA PARTE PARA ALUMNO Y PROFESOR
        		 */
                Query q = pm.newQuery(Cuenta.class);
                q.setFilter("CUENTA == idl");
                q.declareParameters("String idl");
               
                @SuppressWarnings("unchecked")
				List<Cuenta> ids = (List<Cuenta>) q.execute(usu);
                
        		if(!ids.isEmpty()){
        			Cuenta user = ids.get(0);
            	    if(user.isCuenta(usu, pass) && sesion.getAttribute("usuario") == null){
            	    	htmls.session_active(user.getCuenta());
            	    	sesion.setAttribute("user", user);
            	    	if(user.getPersona().getTIPO()==Cuenta.TYPE_ALUMNO)
            	    		response.sendRedirect("/notas");
            	    	else if(user.getPersona().getTIPO()==Cuenta.TYPE_PROFESOR)
            	    		response.sendRedirect("/operprof.jsp");
            	    }else{
            	    	response.setContentType("text/plain");
            	    	response.getWriter().print("contrase�a incorrecta");
            	    }
        		}
            	else{
            		response.setContentType("text/plain");
            		response.getWriter().print("IdLogin Incorrecto");
        		}
        	}
        }catch(Exception e){
        	response.getWriter().println(e);
        //	response.sendRedirect(htmls.ERROR_JSP);
        }finally{
        	pm.close();
        }
    }
 
   //método encargado de la gestión del método GET
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        //me llega la url "proyecto/login/out"
        String action =  request.getRequestURI()!=null?request.getRequestURI():"";
        if(action.equals(htmls.LOGIN)){
        	request.getRequestDispatcher(htmls.LOGIN_JSP).forward(request, response);
        }
        else if(action.equals(htmls.UNLOGIN)){
            HttpSession sesion = request.getSession();
            sesion.invalidate();
            htmls.session_disable();
            response.sendRedirect(htmls.HOME);
        }
        else if(action.equals(htmls.REGISTRER)){
        	request.getRequestDispatcher(htmls.REGISTRAR_JSP).forward(request, response);
        }
        else{
        	response.sendRedirect(htmls.PAGE_NO_FOUND_JSP);
        }
    }
}
