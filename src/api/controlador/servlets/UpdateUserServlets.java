package api.controlador.servlets;

import java.io.IOException;
import java.util.Collection;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import api.controlador.htmls;
import api.modelo.PMF;
import api.modelo.tablas.*;

@SuppressWarnings("serial")
public class UpdateUserServlets extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		String codigo = req.getParameter("codigo");
		int i = codigo.lastIndexOf(':');
		String codCurso = codigo.substring(0, i);
		String codPersona = codigo.substring(i+1, codigo.length());
		
		System.out.println(codPersona);
		System.out.println(codCurso);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Key key;
		try{	
			key = KeyFactory.stringToKey(codCurso);
			Curso c = pm.getObjectById(Curso.class, key);
			key = KeyFactory.stringToKey(codPersona);
			Persona p = pm.getObjectById(Persona.class, key);
			c.setProfesor(p);
			pm.makePersistent(c);
			resp.sendRedirect(htmls.ADMIN_ADMI_ASIGNAR);
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
		}
	}
}
