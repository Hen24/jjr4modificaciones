package api.controlador.servlets;

import java.io.IOException;
import java.util.Collection;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import api.controlador.htmls;
import api.modelo.PMF;
import api.modelo.tablas.*;

@SuppressWarnings("serial")
public class MatrucilarCursosServlets extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		
		String[] elejidos = req.getParameterValues("elegidos");
		String codigoAlum = req.getParameter("codigoAlum");
		
		PersistenceManager pm = PMF.get().getPersistenceManager();

		Key key = KeyFactory.stringToKey(codigoAlum);
		Persona alumno = pm.getObjectById(Persona.class, key);
		
		try{
			if( req.getParameterValues("elegidos")!=null )
				for(String str:elejidos){
					Key keyCurso = KeyFactory.stringToKey(str);
					Curso curso = pm.getObjectById(Curso.class, keyCurso);
					NotasCursos nota = new NotasCursos();
					nota.setAlumno(alumno);
					nota.setCurso(curso);
					pm.makePersistent(nota);
				}
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			Query q = pm.newQuery(Curso.class);
			q.setOrdering("KEY descending");
			try{
				@SuppressWarnings("unchecked")
				Collection cursos = (Collection) q.execute();
				req.setAttribute("cursos", cursos);
				resp.sendRedirect(htmls.ADMIN_ADMI_MATRICULAR);
			}
			catch(Exception e){
				System.out.println(e);
			}
			finally{
				q.closeAll();
			}
		}
		
	}
}
