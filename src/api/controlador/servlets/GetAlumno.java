package api.controlador.servlets;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import javax.jdo.Query;

import api.modelo.PMF;
import api.modelo.tablas.*;

@SuppressWarnings("serial")
public class GetAlumno extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		String alumCod = req.getParameter("alumCod");
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Key key = KeyFactory.stringToKey(alumCod);
		Persona person = pm.getObjectById(Persona.class, key);
		Query q = pm.newQuery(Curso.class);
		try{
			List<Curso> cursos = (List<Curso>) q.execute(); 
			String s = "<table>";
			for(Curso c : cursos){
				s += "<tr>";
				s += "<td><p><input type=\"checkbox\" name=\"elegidos\" value=\""+c.getKey()+"\">"+c.getNombres()+"</p></td>\n";
				s += "</tr>";
			}
			s += "</table>";
			
			resp.getWriter().print(""
					+"<fieldset>"
					+"<form name=\"formCursos\" action=\"/matricularCursos\" method=\"post\">\n"
					+"<input name=\"codigoAlum\" type=\"radio\" value=\""+person.getKey()+"\" checked>Alumno</input><br>"
					+"Codigo   : "+person.getKey()+"<br>\n"
					+"Nombre   : "+person.getNombres()+"<br>\n"
					+"Apellido : "+person.getApellidos()+"<br>\n"
					+"DNI      : "+person.getDni()+"<br>\n"
					+"Cursos: "+ s + "\n"
					+"<input type=\"submit\" value=\"Matricular\">\n"
					+"</form>\n"
					+"</fieldset>"
			 );
		}
		catch(Exception e){
			
		}
		finally{
			q.closeAll();
		}
		
		
	}
}
