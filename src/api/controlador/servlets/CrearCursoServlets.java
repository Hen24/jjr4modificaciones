package api.controlador.servlets;

import java.io.IOException;
import java.util.Collection;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import api.controlador.htmls;
import api.modelo.PMF;
import api.modelo.tablas.*;

@SuppressWarnings("serial")
public class CrearCursoServlets extends HttpServlet{
	@SuppressWarnings("rawtypes")
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		
		String name = req.getParameter("curso");
		String level = req.getParameter("nivel");
		Curso newCurso = new Curso(name, level.substring(1, 2), level.substring(0, 1));
		
		
		
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try{
		pm.makePersistent(newCurso);

		
		Query q = pm.newQuery(Curso.class);
		q.setOrdering("NIVEL ascending");
		
		Collection cursos = (Collection) q.execute();
		req.setAttribute("cursos", cursos);
		req.getRequestDispatcher(htmls.ADMIN_CURSOS_JSP).forward(req, resp);
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			pm.close();
		}
	}
}
