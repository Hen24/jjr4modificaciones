package api.controlador.servlets;

import java.io.IOException;
import java.util.Collection;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import api.controlador.htmls;
import api.modelo.PMF;
import api.modelo.tablas.Cuenta;
import api.modelo.tablas.Persona;

@SuppressWarnings("serial")
public class eliminarUsuarioServlets extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		
		String[] elejidos = req.getParameterValues("elegidos");
		if(elejidos != null){
			for(String str:elejidos){
				System.out.println(str);
			}
		}
		else{
			PersistenceManager pm = PMF.get().getPersistenceManager();
			Query q = pm.newQuery(Cuenta.class);
			q.setOrdering("KEY descending");
			try{
				@SuppressWarnings("unchecked")
				Collection cuentas = (Collection) q.execute();
				req.setAttribute("cuentas", cuentas);
				req.getRequestDispatcher(htmls.ADMIN_USUARIOS_JSP).forward(req, resp);
			}
			catch(Exception e){
				System.out.println(e);
			}
		}
		
		PersistenceManager pm = PMF.get().getPersistenceManager();

		
		try{
			if( req.getParameterValues("elegidos")!=null )
				for(String str:elejidos){
					Key keyPersona = KeyFactory.stringToKey(str);
					Cuenta cuenta = pm.getObjectById(Cuenta.class, keyPersona);
					pm.deletePersistent(cuenta.getPersona());
					pm.deletePersistent(cuenta);
				}
			Query q = pm.newQuery(Cuenta.class);
			q.setOrdering("KEY descending");
			
				@SuppressWarnings("unchecked")
				Collection cuentas = (Collection) q.execute();
				req.setAttribute("cuentas", cuentas);
				req.getRequestDispatcher(htmls.ADMIN_USUARIOS_JSP).forward(req, resp);
			
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			Query q = pm.newQuery(Cuenta.class);
			q.setOrdering("KEY descending");
			try{
				@SuppressWarnings("unchecked")
				Collection cuentas = (Collection) q.execute();
				req.setAttribute("cuentas", cuentas);
				req.getRequestDispatcher(htmls.ADMIN_USUARIOS_JSP).forward(req, resp);
			}
			catch(Exception e){
				System.out.println(e);
			}
			finally{
				q.closeAll();
			}
		}
		
	}
}
