package api.controlador.servlets;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import api.modelo.PMF;
import api.modelo.tablas.Cuenta;
import api.modelo.tablas.NotasCursos;
import api.modelo.tablas.Persona;

@SuppressWarnings("serial")
public class NotasServlet extends HttpServlet{
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException{
				
		HttpSession ses = req.getSession();
		Cuenta c = (Cuenta)ses.getAttribute("user");
		Persona per = c.getPersona();

		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query q = pm.newQuery(NotasCursos.class);
		q.setFilter("ALUMNO == per");
		q.declareParameters("Persona per");
		try{
			List<NotasCursos> lp = (List<NotasCursos>)q.execute(per);
			req.setAttribute("notas", lp);
			RequestDispatcher rd = req.getRequestDispatcher("notas.jsp");
			rd.forward(req, resp);
		}
		catch(Exception e){
			System.out.println(e);	
		}
	}
}
