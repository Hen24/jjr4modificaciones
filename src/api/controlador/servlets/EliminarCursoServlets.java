package api.controlador.servlets;

import java.io.IOException;
import java.util.Collection;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import api.controlador.htmls;
import api.modelo.PMF;
import api.modelo.tablas.*;

@SuppressWarnings("serial")
public class EliminarCursoServlets extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		
		String[] elejidos = req.getParameterValues("elegidos");
		
		if(elejidos != null){
			for(String str:elejidos){
				System.out.println(str);
			}
		}
		
		else{
			PersistenceManager pm = PMF.get().getPersistenceManager();
			Query q = pm.newQuery(Curso.class);
			q.setOrdering("NIVEL ascending");
			try{
				@SuppressWarnings("unchecked")
				Collection cursos = (Collection) q.execute();
				req.setAttribute("cursos", cursos);
				req.getRequestDispatcher(htmls.ADMIN_CURSOS_JSP).forward(req, resp);
			}
			catch(Exception e){
				System.out.println(e);
			}
		}
		
		PersistenceManager pm = PMF.get().getPersistenceManager();

		
		try{
			if( req.getParameterValues("elegidos")!=null )
				for(String str:elejidos){
					Key keyCurso = KeyFactory.stringToKey(str);
					Curso curso = pm.getObjectById(Curso.class, keyCurso);
					pm.deletePersistent(curso);
				}
			Query q = pm.newQuery(Curso.class);
			q.setOrdering("NIVEL ascending");
			
				@SuppressWarnings("unchecked")
				Collection cursos = (Collection) q.execute();
				req.setAttribute("cursos", cursos);
				req.getRequestDispatcher(htmls.ADMIN_CURSOS_JSP).forward(req, resp);
			
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			Query q = pm.newQuery(Curso.class);
			q.setOrdering("NIVEL ascending");
			try{
				@SuppressWarnings("unchecked")
				Collection cursos = (Collection) q.execute();
				req.setAttribute("cursos", cursos);
				req.getRequestDispatcher(htmls.ADMIN_CURSOS_JSP).forward(req, resp);
			}
			catch(Exception e){
				System.out.println(e);
			}
			finally{
				q.closeAll();
			}
		}
		
	}
}
