package api.controlador.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import api.modelo.PMF;
import api.modelo.tablas.Cuenta;
import api.modelo.tablas.Curso;
import api.modelo.tablas.NotasCursos;
import api.modelo.tablas.Persona;

@SuppressWarnings("serial")
public class AgregarNotasServlet extends HttpServlet{
	
	@SuppressWarnings("unchecked")
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException{
		HttpSession ses = req.getSession();
		Cuenta c = (Cuenta)ses.getAttribute("user");
		Persona per = c.getPersona();

		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query q = pm.newQuery(Curso.class);
		q.setFilter("PROFESOR == per");
		q.declareParameters("Persona per");
		try{
			List<Curso> lc = (List<Curso>)q.execute(per);
			req.setAttribute("cursos", lc);
			RequestDispatcher rd = req.getRequestDispatcher("subirNotas.jsp");
			rd.forward(req, resp);
		}
		catch(Exception e){
			System.out.println(e);	
		}
	}
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException{
		
		String curso = req.getParameter("curso");
		String bimestre = req.getParameter("bimestre");
		String[] alumnos = req.getParameterValues("alumnos");
		String[] notas = req.getParameterValues("notas");
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try{
			Key key = KeyFactory.stringToKey(curso);
			Curso curs = pm.getObjectById(Curso.class, key);
			for(int i = 0; i <alumnos.length; i++){

				Key key_al = KeyFactory.stringToKey(alumnos[i]);
				Persona al = pm.getObjectById(Persona.class, key_al);
				Query q = pm.newQuery(NotasCursos.class);
				q.setFilter("ALUMNO == alumno && CURSO == curso");
				q.declareParameters("Persona alumno, Curso curso");
				List<NotasCursos> c = (List<NotasCursos>) q.execute(al, curs);
				switch(bimestre){
				case "1":{
					c.get(0).setNota1(Integer.parseInt(notas[i]));
					break;
				}
				case "2":{
					c.get(0).setNota2(Integer.parseInt(notas[i]));
					break;
				}
				case "3":{
					c.get(0).setNota3(Integer.parseInt(notas[i]));
					break;
				}
				case "4":{
					c.get(0).setNota4(Integer.parseInt(notas[i]));
					break;
				}
				default:{ break;}
				}
			}
			Query q = pm.newQuery(Curso.class);
			q.setFilter("PROFESOR == per");
			q.declareParameters("Persona per");
			try{
				@SuppressWarnings("unchecked")
				List<Curso> lc = (List<Curso>)q.execute(curs.getProfesor());
				req.setAttribute("cursos", lc);
				RequestDispatcher rd = req.getRequestDispatcher("subirNotas.jsp");
				rd.forward(req, resp);
			}
			catch(Exception e){
				System.out.println(e);	
			}//hecho correctamente
		}catch(Exception e){
			System.out.println(e);
			
		}
		//*/
	}
}
