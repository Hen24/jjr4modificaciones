package api.controlador.servlets;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import javax.jdo.Query;

import api.modelo.PMF;
import api.modelo.tablas.*;

@SuppressWarnings("serial")
public class GetCurso extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		String codCurso = req.getParameter("cursoCod");
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Key key = KeyFactory.stringToKey(codCurso);
		Curso curso = pm.getObjectById(Curso.class, key);

		Query q = pm.newQuery(Persona.class);
		q.setOrdering("KEY descending");
		q.setFilter("TIPO == 3");
		@SuppressWarnings("unchecked")
		List<Persona> Personas = (List<Persona>) q.execute();
		
		String select = "<select name=\"codigo\">";
		for(Persona per : Personas){
			select += "<option value=\""+curso.getKey()+":"+per.getKey()+"\">"
					+per.getNombres()
					+":"
					+per.getApellidos()
					+"</option>\n";
		}
		select += "</select>";
		
		resp.getWriter().print(""
				+"<form name=\"formCursos\" action=\"/updateCurso\" method=\"post\">\n"
				+"Curso   : "+curso.getKey()+"<br>\n"
				+"Nombre  : "+curso.getNombres()+"<br>\n"
				+"Profesor: "+select + "\n"
				+"<input type=\"submit\" value=\"Asiganar\">\n"
				+"</form>\n"
		 );
	}
}
