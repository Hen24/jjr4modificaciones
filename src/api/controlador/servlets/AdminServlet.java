package api.controlador.
servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import api.controlador.htmls;
import api.modelo.PMF;
import api.modelo.tablas.*;

@SuppressWarnings("serial")
public class AdminServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		HttpSession sesion = req.getSession();
		Admin admin = (Admin) sesion.getAttribute("admin");
        String action =  req.getRequestURI()!=null?req.getRequestURI():"";
		if(action.equals(htmls.ADMIN_HOME_PAGE)){
			if(admin!=null){
				htmls.session_active("admin");
				req.getRequestDispatcher(htmls.ADMIN_HOME_JSP).forward(req, resp);
			}
			else resp.sendRedirect(htmls.PAGE_DENNY_JSP);
		}
		else if(action.equals(htmls.ADMIN_ADMI_CURSO)){
			if(admin!=null){
				PersistenceManager pm = PMF.get().getPersistenceManager();
				Query q = pm.newQuery(Curso.class);
				q.setOrdering("NIVEL descending");
				try{
					@SuppressWarnings("unchecked")
					Collection cursos = (Collection) q.execute();
					req.setAttribute("cursos", cursos);
					req.getRequestDispatcher(htmls.ADMIN_CURSOS_JSP).forward(req, resp);
				}
				catch(Exception e){
					System.out.println(e);
				}
				finally{
					q.closeAll();
				}
			}
			else resp.sendRedirect(htmls.PAGE_DENNY_JSP);
		}
		else if(action.equals(htmls.ADMIN_ADMI_USUARIOS)){
			if(admin!=null){
				
				PersistenceManager pm = PMF.get().getPersistenceManager();
				Query q = pm.newQuery(Cuenta.class);
				q.setOrdering("KEY descending");
				try{
					@SuppressWarnings("unchecked")
					Collection cuentas = (Collection) q.execute();
					req.setAttribute("cuentas", cuentas);
					req.getRequestDispatcher(htmls.ADMIN_USUARIOS_JSP).forward(req, resp);
				}
				catch(Exception e){
					System.out.println(e);
				}
				finally{
					q.closeAll();
				}
			}
			else resp.sendRedirect(htmls.PAGE_DENNY_JSP);
		}
		else if(action.equals(htmls.ADMIN_ADMI_MATRICULAR)){
		//	if(admin!=null){
				PersistenceManager pm = PMF.get().getPersistenceManager();
				Query q = pm.newQuery(Persona.class);
				q.setFilter("TIPO == 2");
				q.setOrdering("KEY descending");
				try{
					@SuppressWarnings("unchecked")
					Collection alumnos = (Collection) q.execute();
					req.setAttribute("alumnos", alumnos);
					req.getRequestDispatcher(htmls.ADMIN_MATRICULAR_JSP).forward(req, resp);
				}
				catch(Exception e){
					System.out.println(e);
				}
				finally{
					q.closeAll();
				}
		//	}
		//	else resp.sendRedirect(htmls.PAGE_DENNY_JSP);
		}
		
		/*
		else if(action.equals(htmls.ADMIN_ADMI_ASIGNAR)){
			if(admin!=null){
				PersistenceManager pm = PMF.get().getPersistenceManager();
				Query q = pm.newQuery(Curso.class);
				q.setOrdering("KEY descending");
				Query q2 = pm.newQuery(Persona.class);
				q2.setOrdering("KEY descending");
				q2.setFilter("TIPO == 3");
				try{
					@SuppressWarnings("unchecked")
					Collection cursos = (Collection) q.execute();
					req.setAttribute("cursos", cursos);
					Collection profesores = (Collection) q2.execute();
					req.setAttribute("profesores", profesores);
					req.getRequestDispatcher(htmls.ADMIN_ASIGNAR_JSP).forward(req, resp);
				}
				catch(Exception e){
					System.out.println(e);
				}
				finally{
					
				}
			}
			else resp.sendRedirect(htmls.PAGE_DENNY_JSP);
		}
		*/
		///*
		else if(action.equals(htmls.ADMIN_ADMI_ASIGNAR)){
			if(admin!=null){
				PersistenceManager pm = PMF.get().getPersistenceManager();
				Query q = pm.newQuery(Curso.class);
				q.setFilter("ASIGNADO == false");
				q.setOrdering("KEY descending");
				
				Query q1 = pm.newQuery(Curso.class);
				q1.setFilter("ASIGNADO == true");
				q1.setOrdering("KEY descending");
				
				Query q2 = pm.newQuery(Persona.class);
				q2.setOrdering("KEY descending");
				q2.setFilter("TIPO == 3");
				try{
					@SuppressWarnings("unchecked")
					Collection cursosnoasig = (Collection) q.execute();
					req.setAttribute("cursosnoasig", cursosnoasig);
					Collection cursosasig = (Collection) q1.execute();
					req.setAttribute("cursosasig", cursosasig);
					
					Collection profesores = (Collection) q2.execute();
					req.setAttribute("profesores", profesores);
					req.getRequestDispatcher(htmls.ADMIN_ASIGNAR_JSP).forward(req, resp);
				}
				catch(Exception e){
					System.out.println(e);
				}
				finally{
					
				}
			}
			else resp.sendRedirect(htmls.PAGE_DENNY_JSP);
		}
		//*/
		else if(action.equals(htmls.ADMIN_ADMI_CALENDARIO)){
			if(admin!=null){
				/*
				 * calenadrio 
				 * 				 
				 */
			}
			else resp.sendRedirect(htmls.PAGE_DENNY_JSP);
		}
		else if(action.equals(htmls.ADMIN_ADMI_CONTACTENOS)){
			if(admin!=null){
				/*
				 * calenadrio 
				 * 				 
				 */
			}
			else resp.sendRedirect(htmls.PAGE_DENNY_JSP);
		}
		else resp.sendRedirect(htmls.PAGE_NO_FOUND_JSP);
	}

}
