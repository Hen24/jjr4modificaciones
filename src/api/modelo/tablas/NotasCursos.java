package api.modelo.tablas;

import java.io.Serializable;

import javax.jdo.annotations.*;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.datanucleus.annotations.Unowned;

@SuppressWarnings("serial")
@PersistenceCapable
public class NotasCursos implements Serializable{
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key KEY;
	
	@Persistent
	private short MATRICULA;
	
	@Persistent
	@Unowned
	private Curso CURSO;
	
	@Persistent
	@Unowned
	private Persona ALUMNO;
	
	@Persistent
	private int [] NOTAS;
	
	public NotasCursos(){
		NOTAS = new int[4];
	}
	
	public void setKey(String idNotasCurso) {
		Key keyPersona = KeyFactory.stringToKey(idNotasCurso);
		this.KEY = KeyFactory.createKey(keyPersona,
		Persona.class.getSimpleName(), java.util.UUID.randomUUID().toString());
	}
	
	public String getKey() {
		return KeyFactory.keyToString(KEY);
	}
	
	public void setMatricula(short Matricula){this.MATRICULA = Matricula;}
	
	public short getMatricula(){return MATRICULA;}
	
	public void setCurso(Curso c){this.CURSO = c;}
	
	public Curso getCurso(){return CURSO;}
	
	public void setAlumno(Persona a){this.ALUMNO = a;}
	
	public Persona getALumno(){return ALUMNO;}
	
	public void setNota1(int n){this.NOTAS[1] = n;}
	public void setNota2(int n){this.NOTAS[2] = n;}
	public void setNota3(int n){this.NOTAS[3] = n;}
	public void setNota4(int n){this.NOTAS[4] = n;}
	
	public int[] getNotas(){return NOTAS;}
}
