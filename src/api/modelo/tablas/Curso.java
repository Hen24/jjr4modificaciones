package api.modelo.tablas;

import javax.jdo.annotations.*;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.datanucleus.annotations.Unowned;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
@PersistenceCapable
public class Curso implements Serializable{

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key KEY;
	
	@Persistent
	private String NOMBRE_CURSO;
	
	@Persistent
	@Unowned
	private List<NotasCursos> NOTAS_CURSO = new ArrayList<NotasCursos>();
	
	@Persistent
	@Unowned
	private Persona PROFESOR;
	
	@Persistent
	private String NIVEL;
	
	@Persistent
	private String GRADO;
	
	@Persistent 
	private boolean ASIGNADO;
	
	public Curso(String nombre_curso, String nivel, String grado) {
		super();
		this.NOMBRE_CURSO = nombre_curso;
		this.NIVEL = nivel;
		this.GRADO = grado;
		this.PROFESOR = null;
		this.ASIGNADO = false;
	}
	
	public boolean isAssign(){
		return this.ASIGNADO;
	}
	
	public String getNombres() {
		return NOMBRE_CURSO;
	}
	
	public String getNivel(){
		return this.NIVEL;
	}
	
	public String getGrado(){
		return this.GRADO;
	}

	public void setNombres(String Nombres) {
		this.NOMBRE_CURSO = Nombres;
	}
	
	public void setProfesor(Persona p){
		this.PROFESOR = p;
		this.ASIGNADO = true;
	}
	public Persona getProfesor(){
		return PROFESOR;
	}
	
	public void setKey(String idCurso) {
		Key keyPersona = KeyFactory.stringToKey(idCurso);
		this.KEY = KeyFactory.createKey(keyPersona,
		Persona.class.getSimpleName(), java.util.UUID.randomUUID().toString());
	}
	
	public String getKey() {
		return KeyFactory.keyToString(KEY);
	}
	
	public void setCursos(List<NotasCursos> NOTAS_CURSO){
		this.NOTAS_CURSO = NOTAS_CURSO;
	}
	
	public List<NotasCursos> getCursos(){
		return NOTAS_CURSO;
	}
	
	@Override
	public String toString() {
		String resp = KEY + " : " +NOMBRE_CURSO + " : " + PROFESOR;  
		return resp;
	}
}
