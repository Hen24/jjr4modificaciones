package api.modelo.tablas;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.datanucleus.annotations.Unowned;

@SuppressWarnings("serial")
@PersistenceCapable
public class Cuenta implements Serializable{
	
	static public final short TYPE_NO_ASIGNADO = 0;
	static public final short TYPE_ADMIN = 1;
	static public final short TYPE_ALUMNO = 2;
	static public final short TYPE_PROFESOR = 3;
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key KEY;
	
	@Persistent 
	private String CUENTA;
	
	@Persistent 
	private String CONTRASENIA;
	
	@Persistent 
	@Unowned
	private Persona PERSONA;
	
	public Cuenta(String Cuenta,String Contracenia){
		this.CUENTA = Cuenta;
		this.CONTRASENIA = Contracenia;
	}
	
	public void setCuenta(String newCuenta){CUENTA = newCuenta;}
	
	public String getCuenta(){return CUENTA;}
	
	public void setContrasenia(String newContrasenia){CONTRASENIA = newContrasenia;}
	
	public String getContrasenia(){return CONTRASENIA;}
	
	public void setPersona(Persona p){PERSONA = p;}
	
	public Persona getPersona(){return PERSONA;}
	
	public boolean isCuenta(String usu,String pass){
		return CUENTA.equals(usu) && CONTRASENIA.equals(pass);
	}
	
	public short getTIPO(){
		if(PERSONA==null) return TYPE_NO_ASIGNADO;
		else return PERSONA.getTIPO();
	}
	
	public void setKey(String idCuenta) {
		Key keyPersona = KeyFactory.stringToKey(idCuenta);
		this.KEY = KeyFactory.createKey(keyPersona,
		Persona.class.getSimpleName(), java.util.UUID.randomUUID().toString());
	}
	
	public String getKey() {
		return KeyFactory.keyToString(KEY);
	}
	
	@Override
	public String toString() {
		String resp = KEY + " : " +CUENTA + " : " + CONTRASENIA + " : " + PERSONA;  
		return resp;
	}
}
