package api.modelo.tablas;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@SuppressWarnings("serial")
@PersistenceCapable
public class Persona implements Serializable{

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key KEY;
	
	@Persistent
	private String NOMBRE;
	
	@Persistent
	private String APELLIDOS;
	
	@Persistent
	private String DNI;
	
	@Persistent
	protected short TIPO;
	
	public Persona(String Nombres, String Apellidos, String dni) {
		super();
		this.NOMBRE = Nombres;
		this.APELLIDOS = Apellidos;
		this.DNI = dni;
	}

	public String getNombres() {
		return NOMBRE;
	}

	public void setNombres(String Nombres) {
		this.NOMBRE = Nombres;
	}

	public String getApellidos() {
		return APELLIDOS;
	}

	public void setApellidos(String Apellidos) {
		this.APELLIDOS = Apellidos;
	}
	
	public String getDni() {
		return DNI;
	}

	public void setDni(String Dni) {
		this.DNI = Dni;
	}
	
	public void setKey(String idPersona) {
		Key keyPersona = KeyFactory.stringToKey(idPersona);
		this.KEY = KeyFactory.createKey(keyPersona,
		Persona.class.getSimpleName(), java.util.UUID.randomUUID().toString());
	}
	
	public String getKey() {
		return KeyFactory.keyToString(KEY);
	}
	
	public void setTIPO(short type){
		TIPO = type;
	}
	
	public short getTIPO(){return TIPO;}
	
	@Override
	public String toString() {
		String resp = KEY + " : " +NOMBRE + " : " + APELLIDOS;  
		return resp;
	}
}
