package api.modelo.tablas;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
@SuppressWarnings("serial")
@PersistenceCapable
public class Comunicados implements Serializable{
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key KEY;
	
	@Persistent
	private String COMUNICADO;
	
	@Persistent
	private String FECHA;
	//AAAA-MM-DD--HH:MM
	@Persistent
	private String grado;
	@Persistent
	private String titulo;
	
	public Comunicados(String comunicado,String Fecha,String grado,String titulo){
		this.COMUNICADO = comunicado;
		this.FECHA = Fecha;
		this.grado=grado;
		this.titulo=titulo;
	}
	
	public void setKey(String idComunicado) {
		Key keyPersona = KeyFactory.stringToKey(idComunicado);
		this.KEY = KeyFactory.createKey(keyPersona,
		Persona.class.getSimpleName(), java.util.UUID.randomUUID().toString());
	}
	
	public String getKey() {
		return KeyFactory.keyToString(KEY);
	}
	
	public void setTitulo(String t){
		this.titulo = t;
	}
	
	public String getTitulo(){
		return titulo;
	}
	
	public void setComunicado(String Comunicado){
		this.COMUNICADO = Comunicado;
	}
	
	public String getComunicado(){
		return COMUNICADO;
	}
	public void setFecha(String fecha){
		this.FECHA = fecha;
	}
	
	public String getFecha(){
		return FECHA;
	}
	public void setGrado(String grado){
		this.grado = grado;
	}
	
	public String getGrado(){
		return grado;
	}
}
