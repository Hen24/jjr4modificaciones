<%@ page import="api.controlador.htmls"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>index</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="estilos/flexboxgrid.css">
    <link rel="stylesheet" href="estilos/estilo.css">
    <link rel="stylesheet" href="estilos/iconos.css">
    <script src="js/jquery.js"></script>
    <script src="js/control.js"></script>
    <script src="js/jqueryS.js"></script>
    <script src="js/responsiveslides.min.js"></script>
    <script src="main.js"></script>
    <script>
        $(function() {
            $(".rslides").responsiveSlides({
                maxwidth: 800,
                speed: 800
              });
            $("#slider").responsiveSlides({
                pager: false,
                nav: true,
                speed: 800,
                namespace: "callbacks",
                before: function () {
                    $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                    $('.events').append("<li>after event fired.</li>");
                }
            });
        });
    </script>
</head>
<body>
    <header>
        <div class="logo">
            <div>
                <div class="row center-xs">
                    <div class="titulo">
                        <h1>Juan Jacobo Rousseau</h1>
                    </div>
                    <div class="logoc">
                        <div class="logor">
                            <div class="logol">
                            <img src="images/logo.gif" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="menu-resp">
           <a href="#" class="bt-menu"><span class="icon-align-justify"></span>Menu</a>
        </div>
        <nav>
            <div class="menu-p">
                <ul>
                        <li><a href="index.jsp"><span class="icon-home"></span>Inicio</a></li>
                        <li><a href="misionyvision.jsp"><span class="icon-flag"></span>Nuestra Institución</a></li>
                        <li class="submenu"><a href="#"><span class="icon-open-book"></span>Areas<span class="caret icon-chevron-down"></span></a>
                            <ul class="children">
                                <li><a href="inicial.jsp">Inicial</a></li>
                                <li><a href="primaria.jsp">Primaria</a></li>
                                <li><a href="secundaria.jsp">Secundaria</a></li>
                            </ul>
                        </li>
                        <li><a href="galeria.jsp"><span class="icon-camera"></span>Galeria</a></li>
                        <li><a href="contactenos.jsp"><span class="icon-phone"></span>Contactos</a></li>
                        <li><a href="comunicados.jsp"><span class="icon-speech-bubble"></span>Comunicados</a></li>
                        <li><a href="calendario.jsp"><span class="icon-news"></span>Calendario Escolar</a></li>
                        <li><a href="login.jsp"><span class="icon-user"></span>Ingresar</a></li>
                    </ul>
            </div>
        </nav>
    </header>
    <section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 callbacks_container">
              <ul class="rslides" id="slider">
                <li>
                  <img src="images/imgc1.jpg" alt="">
                  <p class="caption">TRABAJANDO UNIDOS POR UNA EDUCACIÓN CONSCIENTE</p>
                </li>
                <li>
                  <img src="images/imgc6.jpg" alt="">
                  <p class="caption">TRABAJANDO UNIDOS POR UNA EDUCACIÓN CONSCIENTE</p>
                </li>
                <li>
                  <img src="images/imgc3.jpg" alt="">
                  <p class="caption">TRABAJANDO UNIDOS POR UNA EDUCACIÓN CONSCIENTE</p>
                </li>
              </ul>
              </div>
        <div class="row cuerpo">
            <div class="col-xs-1">
                
            </div>
            <div class="col-xs-5 texto">
                <h1></h1>
                <p>Nos es grato saludarles para presentar la agenda 2016 de la I.E.P. Juan Jacobo Rousseau, herramienta que esperamos sirva para mantener mayor comunicación entre la comunidad educativa: dirección, maestros, padres de familia y estudiantes; que permita continuar mejorando la calidad educativa de la institución.<br></p>
	            <p>Además nos hemos propuesto sintetizar en ella, la información básica del presente año escolar 2016, con las principales actividades cronogramadas e información necesaria sobre la institución como su visión, misión, objetivos así como algunos puntos relevantes del Reglamento Interno 2016.<br></p>
	            <p>Es nuestro deseo que el presente año académico sea provechoso y que la agenda presentada sea de utilidad para nuestra labor educativa y desempeño académico en general.
	            </p>
            </div>
            <div class="col-xs-1 espacio"></div>
            <div class="col-xs-3 sidebar">
                 <div class="contenedor-img ejemplo-1">  
                 <img src="images/imgfp.jpg">  
                 <div class="mascara">  
                 <h2>Frase del día</h2>  
                 <p>Una de las ventajas de las buenas acciones es la de elevar el alma y disponerla a hacer otras mejores.<br>Juan Jacobo Rousseau</p>
                 </div>  
                </div>
                <div class="contenedor-img ejemplo-1">  
                 <img src="images/imgf.jpg">  
                 <div class="mascara">  
                 <h2>Frase del día</h2>  
                 <p>Enseñarás a volar, pero no volarán tu vuelo. Enseñarás a soñar, pero no soñarán tu sueño. Enseñarás a vivir, pero no vivirán tu vida. Sin embargo… en cada vuelo, en cada vida, en cada sueño, perdurará siempre la huella del camino enseñado.<br>Madre Teresa de Calcuta.</p>
                 </div>  
                </div>
            </div>
            </div>
        </div>
        <div class="row fin-pag">
         <div class="row">
            <div class="col-xs-2">
         <nav>
       <ul>
        <li><a href="index.jsp">Inicio</a></li>
        <li><a href="misionyvision.jsp">Nuestras institución</a></li>
        <li><a href="galeria.jsp">Galería</a></li>
        <li><a href="contactenos.jsp">Contactenos</a></li>
        </ul>
    </nav>
        </div>
        <div class="col-xs-3">  
        </div>
        <div class="col-xs-6 texto2">  
        <h1>Contacto</h1>
        <p>Al correo:<br><br>juan_jacobo_rousseau@hotmail.com</p> 
        </div>
         </div>
        
        
    </div>
    </div>
    
    </section>
</body>
</html>