<%@page import="api.controlador.htmls"%>
<%@page import="java.util.List" %>
<%@page import="api.modelo.tablas.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%
@SuppressWarnings("unchecked")
List<NotasCursos> ln = (List<NotasCursos>)request.getAttribute("lista_notas");
Curso curso = (Curso) request.getAttribute("curso");
int bi=1;
%>
<form action="/agregarNotas" method="post">
	<input hidden name="curso" value="<%=curso.getKey()%>">
	<input hidden name="bimestre" value="<%=bi%>">		
	<table>
	<tr><td colspan="5"><%=curso.getNombres() %></td></tr>
	<tr>
		<th>APELLIDOS Y NOMBRES</th>
		<th>Nota 1</th>
		<th>Nota 2</th>
		<th>Nota 3</th>
		<th>Nota 4</th>
	</tr>
	<%for(NotasCursos nc : ln){ %>
		<tr>
			<td hidden><input name="alumnos" value="<%=nc.getALumno().getKey()%>" ></td>
			<td><%=nc.getALumno().getApellidos()+", "+nc.getALumno().getNombres()%></td>
			<%if(bi==1){%><td><input class="notas" name="notas" value="<%=nc.getNotas()[0]%>"></td>
			<%}else{%><td><%=nc.getNotas()[0]%></td><%}%>
			<%if(bi==2){%><td><input class="notas" name="notas" value="<%=nc.getNotas()[1]%>"></td>
			<%}else{%><td><%=nc.getNotas()[1]%></td><%}%>
			<%if(bi==3){%><td><input class="notas" name="notas" value="<%=nc.getNotas()[2]%>"></td>
			<%}else{%><td><%=nc.getNotas()[2]%></td><%}%>
			<%if(bi==4){%><td><input class="notas" name="notas" value="<%=nc.getNotas()[3]%>"></td>
			<%}else{%><td><%=nc.getNotas()[3]%></td><%}%>
		</tr>
	<%}%>
	</table>
	<input type="submit" value="Submit"/>
</form>