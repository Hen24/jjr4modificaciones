<%@page import="api.controlador.htmls"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Index</title>
	<link rel="stylesheet" href="<%=htmls.ESTILOS_CSS%>">
	<link rel="stylesheet" href="<%=htmls.FLEXBOXGRID_CSS%>">
	<link rel="stylesheet" href="<%=htmls.ICONOS_CSS%>">
	<script src="<%=htmls.JQUERY_JS%>"></script>
	<script src="<%=htmls.RESPONSIVES_LIDES_JS%>"></script>
	<script src="<%=htmls.MAIN_JS%>"></script>
	<script src="<%=htmls.JQUERYFUNCTIONS_JS%>"></script>
</head>
<body>
	<%=htmls.PORTADA%>
	<%=htmls.MENU %>
	<%=htmls.SPACE%>
	<section class="container">
    <div class="row center-xs">
      <div class="col-xs-10">
        <form action="" method="post" class="form-reg">
          <h2 class="tit-form">Llene los siguientes campos</h2>
          <div class="cont-form">
            <input type="text" name="nombre"  placeholder="Nombre"class="input-48"required>
            <input type="text" name="apellidos" placeholder="Apellido"class="input-48"required>
            <input type="text" name="user" placeholder="Usuario"class="input-100"required>
            <input type="text" name="codver" placeholder="Ingrese Código de verificacion"class="input-100"required>
            <input type="text" name="dni" placeholder="DNI"class="input-48"required>
            <input type="email" name="correo" placeholder="Ingrese correo"class="input-48"required>
            <input type="password" name="contraseña" placeholder="Ingrese contraseña"class="input-100"required>
            <input type="submit" value="Registrar" class="bt-enviar">
          </div>
        </form>
      </div>
    </div>
	</section>
	<%=htmls.SPACE%>
	<%=htmls.FOOTER%>
</body>
</html>