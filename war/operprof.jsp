<%@page import="api.controlador.htmls"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>index</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="estilos/flexboxgrid.css">
    <link rel="stylesheet" href="estilos/estilo.css">
    <link rel="stylesheet" href="estilos/iconos.css">
</head>
<body>
    <header>
        <div class="logo">
            <div>
                <div class="row center-xs">
                    <div class="titulo">
                        <h1>Juan Jacobo Rousseau</h1>
                    </div>
                    <div class="logoc">
                        <div class="logor">
                            <div class="logol">
                            <img src="images/logo.gif" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section class="container">
	     <div class="row center-xs">
	         <div class="col-xs-12">
                     <div class="form-reg">
                         <form action="/agregarNotas" method="get" >
                     <h2 class="tit-form2"> Seleccione una opción</h2>
                     <div class="cont-form">
                         <input type="submit" value="Ingresar Notas" class="bt-enviar">
                     </div> 
	                 </form>
	                 <form action="/añadircomunicado">
                     <div class="cont-form">
	                     <input type="submit" value="Hacer un comunicado" class="bt-enviar">
	                 </div> 
	                 </form>
                     </div>
	                 
	         </div>
	     </div>
	</section>
</body>
</html>