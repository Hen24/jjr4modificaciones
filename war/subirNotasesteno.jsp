<%@page import="api.controlador.htmls"%>
<%@page import="java.util.List" %>
<%@page import="api.modelo.tablas.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Login</title>
	<link rel="stylesheet" href="<%=htmls.ESTILOS_CSS%>">
	<link rel="stylesheet" href="<%=htmls.FLEXBOXGRID_CSS%>">
	<link rel="stylesheet" href="<%=htmls.ICONOS_CSS%>">
	<link rel="stylesheet" href="/estilos/style.css">
	<script src="<%=htmls.JQUERY_JS%>"></script>
	<script src="<%=htmls.RESPONSIVES_LIDES_JS%>"></script>
	<script src="<%=htmls.MAIN_JS%>"></script>
	<script src="<%=htmls.JQUERYFUNCTIONS_JS%>"></script>
	<script>
	$(document).ready(function() {
		$(".curso").click(function(event) {
			var curso_var = $(this).children().eq(0).text();
			$.post('/getAlumnosNotas',{
				curso : curso_var
			},function(responseText) {
				$('#lista_notas').html(responseText);
			});
		});
	});
	</script>
</head>
<%
@SuppressWarnings("unchecked")
List<Curso> cursos = (List<Curso>)request.getAttribute("cursos");
%>
<body>
	<%=htmls.PORTADA%>
	<%=htmls.getMenu() %>
	<%=htmls.SPACE%>
	<section class="container">
	     <div class="row center-xs">
	        <div class="col-xs-10">
	        <table>
	        	<tr>
					<td colspan="5"></td>
					<td colspan="5">NOMBRE</td>
					<td colspan="5">NIVEL</td>
					<td colspan="5">GRADO</td>
				</tr>
				<%for(Curso c:cursos){%>
				<tr class="curso">
					<td colspan="5"><%=c.getKey() %></td>
					<td colspan="5"><%=c.getNombres()%></td>
					<td colspan="5"><%=c.getNivel()%></td>
					<td colspan="5"><%=c.getGrado()%></td>
				</tr>
				<%} %>
			</table>
			<div id="lista_notas"></div>	
				
	       </div>
	   </div>
	</section>
	<%=htmls.SPACE%>
	<%=htmls.FOOTER%>
</body>
</html>