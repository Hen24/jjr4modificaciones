<%@page import="api.controlador.htmls"%>
<%@page import="java.util.List" %>
<%@page import="api.modelo.tablas.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Login</title>
	<link rel="stylesheet" href="<%=htmls.ESTILOS_CSS%>">
	<link rel="stylesheet" href="<%=htmls.FLEXBOXGRID_CSS%>">
	<link rel="stylesheet" href="<%=htmls.ICONOS_CSS%>">
	<script src="<%=htmls.JQUERY_JS%>"></script>
	<script src="<%=htmls.RESPONSIVES_LIDES_JS%>"></script>
	<script src="<%=htmls.MAIN_JS%>"></script>
	<script src="<%=htmls.JQUERYFUNCTIONS_JS%>"></script>
</head>
<body>

<%
List<NotasCursos> nt = (List<NotasCursos>)request.getAttribute("notas");
%>
	<%=htmls.PORTADA%>
	<%=htmls.getMenu() %>
	<%=htmls.SPACE%>
	<section class="container">
	     <div class="row center-xs">
	         <div class="col-xs-10">
	       	<table>
				<%for(NotasCursos n:nt){ %>
				<tr>
				<td><%=n.getCurso().getNombres()%></td>
				<%int[] a = n.getNotas();%>
				<td><%=a[0]%></td>
				<td><%=a[1]%></td>
				<td><%=a[2]%></td>
				<td><%=a[3]%></td>
				<%} %>
				</tr>
				</table>
	       </div>
	   </div>
	</section>
	<%=htmls.SPACE%>
	<%=htmls.FOOTER%>


</body>
</html>