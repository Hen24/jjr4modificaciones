// wait for the DOM to be loaded 

$(document).ready(function() {
	$(".rowCursos").click(function(event) {
		var codigo = $(this).children().eq(0).text();
		$.post('/getcurso',{
			cursoCod : codigo
		},function(responseText) {
			$('#formulario').html(responseText);
		});
	});
});

$(document).ready(function() {
	$(".rowAlum").click(function(event) {
		var codigo = $(this).children().eq(0).text();
		alert(codigo);
		$.post('/getAlumno',{
			alumCod : codigo
		},function(responseText) {
			$('#formulario').html(responseText);
		});
	});
});

$(function() {
	$('#limpiarCode').click(function(event) {
		alert("Hello! I am an alert box!!");
		$.get('/AdminDatos',{
			action : "limpiar"
		},function(responseText) {
			$('#tabla').html(responseText);
		});
	});
});

$(function() {
	$('#crearNuevoCurso').click(function(event) {
		var codigoCurso_VAR = $('#codigoCurso').val();
		var nombreCurso_VAR = $('#nombreCurso').val();
		$.get('/AdminDatos',{
				action : "crearCurso",
				nombreCurso : nombreCurso_VAR
		},function(responseText) {
			$('#tabla').html(responseText);
		});
	});
});

$(function(){
	$('.modCurso').click(function(){
		var cod = $(this).parent().siblings().first().text();
		var nomCur = $(this).parent().siblings().eq(1).children().val();
		$.get('/AdminDatos',{
			action : "modCurso",
			codCurso : cod,
			nomCurso : nomCur
		},function(responseText) {
			$('#tabla').html(responseText);
		});
	});
});
$(function(){
	$('.borCurso').click(function(){
		var cod = $(this).parent().siblings().first().text();
		$.get('/AdminDatos',{
			action : "borCurso",
			codCurso : cod
		},function(responseText) {
			$('#tabla').html(responseText);
		});
		
	});
});
