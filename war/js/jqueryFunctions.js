

$(function() {
    $(".rslides").responsiveSlides({
        maxwidth: 800,
        speed: 800
      });
    $("#slider").responsiveSlides({
        pager: false,
        nav: true,
        speed: 800,
        namespace: "callbacks",
        before: function () {
            $('.events').append("<li>before event fired.</li>");
        },
        after: function () {
            $('.events').append("<li>after event fired.</li>");
        }
    });
});
