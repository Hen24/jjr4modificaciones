<%@page import="api.controlador.htmls"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>index</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="estilos/flexboxgrid.css">
    <link rel="stylesheet" href="estilos/estilo.css">
    <link rel="stylesheet" href="estilos/iconos.css">
    <script src="js/jquery.js"></script>
    <script src="js/control.js"></script>
</head>
<body>
    <header>
        <div class="logo">
            <div>
                <div class="row center-xs">
                    <div class="titulo">
                        <h1>Juan Jacobo Rousseau</h1>
                    </div>
                    <div class="logoc">
                        <div class="logor">
                            <div class="logol">
                            <img src="images/logo.gif" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="menu-resp">
           <a href="#" class="bt-menu"><span class="icon-align-justify"></span>Menu</a>
        </div>
        <nav>
            <div class="menu-p">
                <ul>
                        <li><a href="index.jsp"><span class="icon-home"></span>Inicio</a></li>
                        <li><a href="misionyvision.jsp"><span class="icon-flag"></span>Nuestra Institución</a></li>
                        <li class="submenu"><a href="#"><span class="icon-open-book"></span>Areas<span class="caret icon-chevron-down"></span></a>
                            <ul class="children">
                                <li><a href="inicial.jsp">Inicial</a></li>
                                <li><a href="primaria.jsp">Primaria</a></li>
                                <li><a href="secundaria.jsp">Secundaria</a></li>
                            </ul>
                        </li>
                        <li><a href="galeria.jsp"><span class="icon-camera"></span>Galeria</a></li>
                        <li><a href="contacto.jsp"><span class="icon-phone"></span>Contactos</a></li>
                        <li><a href="comunicados.jsp"><span class="icon-speech-bubble"></span>Comunicados</a></li>
                        <li><a href="calendario.jsp"><span class="icon-news"></span>Calendario Escolar</a></li>
                        <li><a href="login.jsp"><span class="icon-user"></span>Ingresar</a></li>
                    </ul>
            </div>
        </nav>
    </header>
    <section>
    <div class="container">
        <div class="row center-xs"><b>Una de las ventajas de las buenas acciones es la de elevar el alma y disponerla a hacer otras mejores.</b></div>
        <div class="row">
            <div class="col-xs-12" id="infor">
                <ul class="galeria">
                    <li class="galeria-item"><img src="images/imgc1.jpg" alt="" class="galeria-img" id="img1"></li>
                    <li class="galeria-item"><img src="images/imgc2.jpg" alt="" class="galeria-img" id="img2"></li>
                    <li class="galeria-item"><img src="images/imgc3.jpg" alt="" class="galeria-img" id="img3"></li>
                    <li class="galeria-item"><img src="images/imgc4.jpg" alt="" class="galeria-img" id="img4"></li>
                    <li class="galeria-item"><img src="images/imgc5.jpg" alt="" class="galeria-img" id="img5"></li>
                    <li class="galeria-item"><img src="images/imgc6.jpg" alt="" class="galeria-img" id="img6"></li>
                    <li class="galeria-item"><img src="images/imgc7.jpg" alt="" class="galeria-img" id="img7"></li>
                    <li class="galeria-item"><img src="images/imgc8.jpg" alt="" class="galeria-img" id="img8"></li>
                    <li class="galeria-item"><img src="images/imgc9.jpg" alt="" class="galeria-img" id="img9"></li>
                    <li class="galeria-item"><img src="images/imgc10.jpg" alt="" class="galeria-img" id="img10"></li>
                    <li class="galeria-item"><img src="images/imgc11.jpg" alt="" class="galeria-img" id="img11"></li>
                    <li class="galeria-item"><img src="images/imgc12.jpg" alt="" class="galeria-img" id="img12"></li>
                    <li class="galeria-item"><img src="images/imgc13.jpg" alt="" class="galeria-img" id="img13"></li>
                    <li class="galeria-item"><img src="images/imgc14.jpg" alt="" class="galeria-img" id="img14"></li>
                    <li class="galeria-item"><img src="images/imgc15.jpg" alt="" class="galeria-img" id="img15"></li>
                    <li class="galeria-item"><img src="images/imgc16.jpg" alt="" class="galeria-img" id="img16"></li>
                    
                </ul>
                <script src="js/main.js"></script>
            </div>
        </div>
        <div class="row fin-pag">
         <div class="row">
            <div class="col-xs-2">
         <nav>
       <ul>
        <li><a href="index.jsp">Inicio</a></li>
        <li><a href="misionyvision.jsp">Nuestras institución</a></li>
        <li><a href="galeria.jsp">Galería</a></li>
        <li><a href="contactenos.jsp">Contactenos</a></li>
        </ul>
    </nav>
        </div>
        <div class="col-xs-3">  
        </div>
        <div class="col-xs-6 texto2">  
        <h1>Contacto</h1>
        <p>Al correo:<br><br>juan_jacobo_rousseau@hotmail.com</p> 
        </div>
         </div>
        
        
    </div>
    </div>
    </section>
</body>
</html>