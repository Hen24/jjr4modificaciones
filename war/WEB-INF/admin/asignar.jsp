<%@page import="api.controlador.htmls"%>
<%@page import="api.modelo.tablas.*"%>
<%@ page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Administrador</title>
<script src="<%=htmls.JQUERY_LATEST_JS%>"></script>
<script src="/js/jqueryFunctionsAdmin.js"></script>
</head>
<%
List<Curso> cursosasig = (List<Curso>) request.getAttribute("cursosasig");
List<Curso> cursosnoasig = (List<Curso>) request.getAttribute("cursosnoasig");
List<Persona> profesores = (List<Persona>) request.getAttribute("profesores");
%>
<body>
	<%=htmls.getMenu_Admin() %>
	<div id="formulario">
		<form name="formCursos" action="/asignarcursoprofesor" method="post">
		<select name="prof">
		<%for(Persona p:profesores) {%>
			<option value=<%=p.getKey()%>><%=p.getNombres() +" "+ p.getApellidos()%></option>
		<%} %>
		</select>
		<table border=1 cellspacing=1 cellpadding=5>
		<tr>
				<td><p></p></td>
				<td><p>Nombre</p></td>
				<td><p>Nivel</p></td>
				<td><p>Grado</p></td>
		</tr>
		<%for(Curso ce:cursosnoasig) {%>
		<tr>
			<td><p><input type="checkbox" name="elegidos" value="<%=ce.getKey()%>"></p></td>
			<td><%= ce.getNombres() %></td>
			<%if(ce.getNivel().equals("i")){%><td>Inicial</td>
			<%}else{
				if(ce.getNivel().equals("p")){%><td>Primaria</td>
				<%}else{%><td>Secundaria</td>
			<%}} 
			switch (ce.getGrado().charAt(0)){
					case 'i' : {%>
						<td>Inicial</td>
						<%break; }
					case '1' :{%>
						<td>Primero</td>
						<%break; }
					case '2' :{%>
						<td>Segundo</td>
						<%break; } 
					case '3' :{%>
						<td>Tercero</td>
						<%break; }
					case '4' :{%>
						<td>Cuarto</td>
						<%break; } 
					case '5' :{%>
						<td>Quinto</td>
						<%break; } 
					case '6' :{%>
						<td>Sexto</td>
						<%break; } 
					} %>
		</tr>
		<%} %>
		</table>
		<input type="submit">
			
		</form>
	</div>
	
	<fieldset><legend>Lista de Cursos ya asignadod (<%=cursosasig.size() %>encontradas)</legend>
		<table>
			<tr>
				<td><p>Curso</p></td>
				<td><p>Nivel</p></td>
				<td><p>Grado</p></td>
				<td><p>Profesor</p></td>
			</tr>
			<%for( Curso c : cursosasig ) {%>
				<tr>
					
					<td><%= c.getNombres() %></td>
			<%if(c.getNivel().equals("i")){%><td>Inicial</td>
			<%}else{
				if(c.getNivel().equals("p")){%><td>Primaria</td>
				<%}else{%><td>Secundaria</td>
			<%}} 
			switch (c.getGrado().charAt(0)){
					case 'i' : {%>
						<td>Inicial</td>
						<%break; }
					case '1' :{%>
						<td>Primero</td>
						<%break; }
					case '2' :{%>
						<td>Segundo</td>
						<%break; } 
					case '3' :{%>
						<td>Tercero</td>
						<%break; }
					case '4' :{%>
						<td>Cuarto</td>
						<%break; } 
					case '5' :{%>
						<td>Quinto</td>
						<%break; } 
					case '6' :{%>
						<td>Sexto</td>
						<%break; } 
					} %>
					
					
					<%if(c.getProfesor()==null){%>
						<td>SIN PROFESOR</td>
					<%}else{ %>
						<td><%=c.getProfesor().getNombres()%></td>
					<%}%>			
				</tr>
			<%}%>
		</table>
	</fieldset>
   
</body>
</html>