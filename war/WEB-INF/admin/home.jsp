<%@page import="api.controlador.htmls"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Administrador</title>
<script src="<%=htmls.JQUERY_LATEST_JS%>"></script>
<script src="/js/jqueryFunctionsAdmin.js"></script>
</head>
<body>
	<%=htmls.getMenu_Admin() %>
</body>
</html>