<%@page import="api.controlador.htmls"%>
<%@page import="api.modelo.tablas.*"%>
<%@ page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Administrador</title>
<script src="<%=htmls.JQUERY_LATEST_JS%>"></script>
<script src="/js/jqueryFunctionsAdmin.js"></script>
</head>
<%
List<Curso> cursos = (List<Curso>) request.getAttribute("cursos");
List<Persona> profesores = (List<Persona>) request.getAttribute("profesores");
%>
<body>
	<%=htmls.getMenu_Admin() %>
	<div id="formulario">
		<form name="formCursos" action="/crearCurso" method="post">
		</form>
	</div>
	
	<fieldset><legend>Lista de Cuentas (<%= cursos.size() %> encontradas)</legend>
		<table>
			<tr>
				<td><p>Codigo</p></td>
				<td><p>Curso</p></td>
				<td><p>Profesor</p></td>
			</tr>
			<%for( Curso c : cursos ) {%>
				<tr class="rowCursos">
					<td><%=c.getKey()%></td>
					<td><%= c.getNombres() %></td>
					<%if(c.getProfesor()==null){%>
						<td>SIN PROFESOR</td>
					<%}else{ %>
						<td><%=c.getProfesor().getNombres()%></td>
					<%}%>			
				</tr>
			<%}%>
		</table>
	</fieldset>
   
</body>
</html>