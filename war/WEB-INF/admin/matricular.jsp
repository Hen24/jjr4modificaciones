<%@page import="api.controlador.htmls"%>
<%@page import="api.modelo.tablas.*"%>
<%@ page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Administrador</title>
<script src="<%=htmls.JQUERY_LATEST_JS%>"></script>
<script src="/js/jqueryFunctionsAdmin.js"></script>
</head>
<%
List<Persona> alumnos = (List<Persona>) request.getAttribute("alumnos");
%>
<body>
	<%=htmls.getMenu_Admin() %>
	<div id="formulario">
		<form name="formCursos" action="#" method="post">
		</form>
	</div>
	<fieldset><legend>Lista de Alumnos (<%= alumnos.size() %> encontradas)</legend>
		<table>
			<tr>
				<td><p>Codigo</p></td>
				<td><p>Alumno</p></td>
				<td><p>DNI</p></td>
			</tr>
			<%for( Persona a : alumnos ) {%>
				<tr class="rowAlum">
					<td><%=a.getKey()%></td>
					<td><%=a.getNombres()%></td>
					<td><%=a.getDni()%></td>	
				</tr>
			<%}%>
		</table>
	</fieldset>
</body>
</html>