<%@page import="api.controlador.htmls"%>
<%@page import="api.modelo.tablas.*"%>
<%@ page import="java.util.List"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Administrador</title>
	<link rel="stylesheet" href="estilos/flexboxgrid.css">
    <link rel="stylesheet" href="estilos/estilo.css">
    <link rel="stylesheet" href="estilos/iconos.css">
<script src="<%=htmls.JQUERY_LATEST_JS%>"></script>
<script src="/js/jqueryFunctionsAdmin.js"></script>
</head>

<%
List<Cuenta> cuentas = (List<Cuenta>) request.getAttribute("cuentas");
%>

<body>
	<%=htmls.getMenu_Admin() %>
	<form name="formUsuario" action="/crearUsuario" method="post">
	    <h2 >Llene los siguientes campos</h2>
	    <div>
	      <input type="text" name="nombre"  placeholder="Nombres" required><br>
	      <input type="text" name="apellidos" placeholder="Apellidos" required><br>
	      <input type="text" name="dni" placeholder="DNI" required><br>
		  <select name="tipo" size="1">
			<option value="<%=Cuenta.TYPE_PROFESOR%>">Profesor</option>
			<option value="<%=Cuenta.TYPE_ALUMNO%>">Alumno</option>			
		  </select><br>
		  <select name="grado">
		  	<option value="ii">Inicial</option>
		  	<option value="1p">1ro Primaria </option>
		  	<option value="2p">2do Primaria </option>
		  	<option value="3p">3ro Primaria </option>
		  	<option value="4p">4to Primaria </option>
		  	<option value="5p">5to Primaria </option>
		  	<option value="6p">6to Primaria </option>
		  	<option value="1s">1ro Secundaria </option>
		  	<option value="2s">2do Secundaria </option>
		  	<option value="3s">3ro Secundaria </option>
		  	<option value="4s">4to Secundaria </option>
		  	<option value="5s">5to Secundaria </option>
		  </select>
	      <input type="submit" value="Registrar" class="bt-enviar"><br>
	    </div>
    </form>
    <form name="listaPersonas" id="listaPersonas" method="post" action="/eliminarUsuario">
    <fieldset><legend>Lista de Cuentas (<%= cuentas.size() %> encontradas)</legend>
		<table border=1 cellspacing=0 cellpadding=5>
			<tr>
				<td><p></p></td>
				<td><p>Nombre</p></td>
				<td><p>Apellido</p></td>
				<td><p>DNI</p></td>
				<td><p>Usuario</p></td>
				<td><p>Pass</p></td>
				<td><p>Tipo</p></td>
			</tr>
			<%for( Cuenta c : cuentas ) {%>
				<tr>
					<td><p><input type="checkbox" name="elegidos" value="<%=c.getKey()%>"></p></td>
					<td><%= c.getPersona().getNombres() %></td>
					<td><%= c.getPersona().getApellidos() %></td>
					<td><%= c.getPersona().getDni() %></td>
					<td><%= c.getCuenta() %></td>
					<td><%= c.getContrasenia() %></td>
					<%if(c.getPersona().getTIPO()==Cuenta.TYPE_PROFESOR) {%>
						<td>Profesor</td>
					<%}else if(c.getPersona().getTIPO()==Cuenta.TYPE_ALUMNO){%>
						<td>Alumno</td>
					<%}%>	
				</tr>
			<%}%>
			<tr>
			<td colspan="7"><input type="submit" value="Eliminar" /></td>
			</tr>
		</table>
	</fieldset>
    </form>
</body>
</html>