<%@page import="api.controlador.htmls"%>
<%@page import="api.modelo.tablas.*"%>
<%@ page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Administrador</title>
<script src="<%=htmls.JQUERY_LATEST_JS%>"></script>
<script src="/js/jqueryFunctionsAdmin.js"></script>
</head>

<%
List<Curso> cursos = (List<Curso>) request.getAttribute("cursos");
%>

<body>
	<%=htmls.getMenu_Admin() %>
	
	<form name="formCursos" action="/crearCurso" method="post">
	    <h2 >Llene los siguientes campos</h2>
	    <div >
	      <input type="text" name="curso" placeholder="Curso"required><br>
	      <select name="nivel">
	      	<option value="ii">Inicial</option>
		  	<option value="1p">1ro Primaria </option>
		  	<option value="2p">2do Primaria </option>
		  	<option value="3p">3ro Primaria </option>
		  	<option value="4p">4to Primaria </option>
		  	<option value="5p">5to Primaria </option>
		  	<option value="6p">6to Primaria </option>
		  	<option value="1s">1ro Secundaria </option>
		  	<option value="2s">2do Secundaria </option>
		  	<option value="3s">3ro Secundaria </option>
		  	<option value="4s">4to Secundaria </option>
		  	<option value="5s">5to Secundaria </option>
	      </select>
	      <input type="submit" value="Crear" class="bt-enviar"><br>
	    </div>
    </form>
	<form name="listaCursos" id="listaPersonas" method="post" action="/eliminarCurso">
    <fieldset><legend>Lista de Cuentas (<%= cursos.size() %> encontradas)</legend>
		<table border=1 cellspacing=0 cellpadding=5>
			<tr>
				<td><p></p></td>
				<td><p>Curso</p></td>
				<td><p>Nivel</p></td>
				<td><p>Grado</p></td>
				<td><p>Profesor</p></td>
			</tr>
			<%for( Curso c : cursos ) {%>
				<tr>
					<td><p><input type="checkbox" name="elegidos" value="<%=c.getKey()%>"></p></td>
					<td><%= c.getNombres() %></td>
					<%if(c != null && c.getNivel().equals("p")){%>
						<td>Primaria</td>
					<%}
					else{
						if(c != null && c.getNivel().equals("s")){%>
						<td>Secundaria</td>
					<%}
					else{%>
						<td>Inicial</td>
					<%}}
						switch (c.getGrado().charAt(0)){
					case 'i' : {%>
						<td>Inicial</td>
						<%break; }
					case '1' :{%>
						<td>Primero</td>
						<%break; }
					case '2' :{%>
						<td>Segundo</td>
						<%break; } 
					case '3' :{%>
						<td>Tercero</td>
						<%break; }
					case '4' :{%>
						<td>Cuarto</td>
						<%break; } 
					case '5' :{%>
						<td>Quinto</td>
						<%break; } 
					case '6' :{%>
						<td>Sexto</td>
						<%break; } 
					} 
					if(c.getProfesor()==null){%>
						<td>SIN PROFESOR</td>
					<%}else{ %>
						<td><%=c.getProfesor().getNombres()%></td>
					<%}%>			
				</tr>
			<%}%>
			<tr>
			<td colspan="7"><input type="submit" value="Eliminar" /></td>
			</tr>
		</table>
	</fieldset>
    </form>
</body>
</html>